import 'dart:math';

import 'package:flutter/material.dart';

class MyProvider extends ChangeNotifier {
  var random = Random();

  int _counterOne = 0;
  int _counterTwo = 0;


  int get counterOne => _counterOne;
  int get counterTwo => _counterTwo;
  void increment() {
    if (random.nextInt(2) == 0) {
      _counterOne++;
    } else {
      _counterTwo++;
    }

    notifyListeners();
  }



  void decrement() {
    if (random.nextInt(2) == 0) {
      _counterOne--;
    } else {
      _counterTwo--;
    }
    notifyListeners();
  }
}
