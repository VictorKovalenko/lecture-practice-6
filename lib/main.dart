import 'package:flutter/material.dart';
import 'package:lecture_practice_6/provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => MyProvider(),
      child: MaterialApp(
        title: 'Lecture Practice 6',
        theme: ThemeData.dark(),
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AppBar'),
      ),
      body: Column(
        children: [
          const SizedBox(height: 200.0),
          Consumer<MyProvider>(
            builder: (context, value, _) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(50, 0, 50, 0),
                    child: Text('${value.counterOne}'),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(50, 0, 50, 0),
                    child: Text('${value.counterTwo}'),
                  ),
                ],
              );
            },
          ),
          const SizedBox(height: 100.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Consumer<MyProvider>(
                builder: (context, value, _) {
                  return Container(
                    padding: EdgeInsets.fromLTRB(50, 0, 50, 0),
                    child: ElevatedButton(
                      child: Text('-'),
                      onPressed: () {
                        value.decrement();
                      },
                    ),
                  );
                },
              ),
              Consumer<MyProvider>(
                builder: (context, value, _) {
                  return Container(
                    padding: EdgeInsets.fromLTRB(50, 0, 50, 0),
                    child: ElevatedButton(
                      child: Text('+'),
                      onPressed: () {
                        value.increment();
                      },
                    ),
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
